import threading

import cv2

from sta.interacts import Interact
from sta.definitions import *


class BaseCascadeInteract(Interact, threading.Thread):

    def startup(self):
        self.start()

    def halt(self):
        self.stop()

    def __init__(self, thread_id, camera_index, cascade):
        Interact.__init__(self)
        threading.Thread.__init__(self)
        self.thread_id = thread_id
        self.is_running = True
        self.should_show_webcam = True
        self.camera_index = camera_index
        self.cascade = cascade

        self.right_boundary = None
        self.left_boundary = None
        self.turbo_boundary = 130
        self.break_boundary = 90

        self.matrix1 = None
        self.init_matrix()

        self.matched_squares = None
        self.origin = None
        self.o_x = 0
        self.o_y = 0
        self.o_wid = 0
        self.init_capture_vars()

        self.f_x = 0
        self.f_wid = 0

        self.focal_x = 0

        self.is_boosting = False
        self.is_breaking = False

    def set_boundaries(self, right_boundary, left_boundary, turbo_boundary):
        self.right_boundary = right_boundary
        self.left_boundary = left_boundary
        self.turbo_boundary = turbo_boundary

    def init_capture_vars(self):
        self.cascade = cv2.CascadeClassifier(self.cascade)
        # self.cascade = cv2.CascadeClassifier(
        #     "assets/cascade/haarcascade_upperbody.xml")
        self.matched_squares = []  # var that stores face rect coords
        self.origin = []  # var that will store the origin coords
        self.o_x = 0
        self.o_y = 0
        self.o_wid = 0

    def set_show_webcam(self, should_show):
        self.should_show_webcam = should_show

    def init_matrix(self):
        self.matrix1 = [[0 for x in xrange(4)] for x in xrange(1)]
        self.matrix1[0][0] = 0
        self.matrix1[0][1] = 0
        self.matrix1[0][2] = 0
        self.matrix1[0][3] = 0

    def detect_patterns(self, image):
        all_detected = []
        detected = self.cascade.detectMultiScale(
            image, 1.3, 4, cv2.cv.CV_HAAR_SCALE_IMAGE, (20, 20))
        if detected != []:
            for x, y, w, h in detected:
                all_detected.append((x, y, w, h))
        return all_detected

    def check_interaction(self):
        # print self.f_x
        # print self.f_wid
        # print self.turbo_boundary
        # print

        if not self.is_detecting():
            self.listener.listen(GameDefinitions.COMMAND_SIGNAL_LOST)
        else:
            self.listener.listen(GameDefinitions.COMMAND_SIGNAL_FOUND)

        if self.f_wid > self.turbo_boundary and not self.is_boosting:
            # print "BOOOST"
            self.listener.listen(GameDefinitions.COMMAND_BOOST)
            self.is_boosting = True
        elif self.f_wid <= self.turbo_boundary and self.is_boosting:
            # print "UNBOOOST"
            self.listener.listen(GameDefinitions.COMMAND_BOOST_RELEASE)
            self.is_boosting = False
        elif self.break_boundary > self.f_wid and not self.is_breaking:
            # print "BREAK!"
            self.listener.listen(GameDefinitions.COMMAND_BREAK)
            self.is_breaking = True
        elif self.f_wid > self.break_boundary and self.is_breaking:
            # print "UNBREAK!"
            self.listener.listen(GameDefinitions.COMMAND_BREAK_RELEASE)
            self.is_breaking = False
        # elif self.turbo_boundary > self.f_wid > self.break_boundary:
            # print "NORMAL!"
            # self.is_boosting = False
            # self.is_breaking = False

    def is_detecting(self):
        return self.matched_squares != []

    def stop(self):
        self.is_running = False

    def run(self):
        # print('Running Cascade Interact')
        i = 0
        capture = cv2.VideoCapture(self.camera_index)
        capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 320)
        capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 240)
        self.f_x = 0
        self.f_wid = 0
        while self.is_running:
            # print(self.f_wid)
            retval, image = capture.read()

            if i % 3 == 0:
                self.matched_squares = self.detect_patterns(image)

            if i % 25 == 0:
                if self.matched_squares != []:
                    self.matrix1[0][0] = self.matched_squares[0][0]
                    self.matrix1[0][1] = self.matched_squares[0][1]
                    self.matrix1[0][2] = self.matched_squares[0][2]
                    self.matrix1[0][3] = self.matched_squares[0][3]

                    self.o_x = (self.matrix1[0][0] + self.matrix1[0][2])/2
                    self.o_y = (self.matrix1[0][1] + self.matrix1[0][3])/2
                    self.o_wid = self.matrix1[0][2]

            if self.o_x != 0 and self.matched_squares != []:
                # c = self.matched_squares[0][0] + self.matched_squares[0][2]
                # self.f_x = c / 2
                self.f_x = self.matched_squares[0][0]
                self.f_wid = self.matched_squares[0][2]

                self.focal_x = self.f_x + (self.f_wid / 2)

                if self.right_boundary is None:
                    self.right_boundary = self.f_x + (self.f_wid * 2 / 3)

                if self.left_boundary is None:
                    self.left_boundary = self.f_x + (self.f_wid * 1 / 3)

            if self.should_show_webcam:
                self.print_window(image)

            i += 1
            if i > 100:
                i = 0

    def print_window(self, image):
        cv2.namedWindow("preview")
        f_y = 50
        f_h = 50

        cv2.rectangle(image, (self.left_boundary, 0), (self.left_boundary + 1, 0 + 240), (0, 0, 255), 2)
        cv2.rectangle(image, (self.right_boundary, 0), (self.right_boundary + 1, 0 + 240), (0, 0, 0), 2)

        # cv2.rectangle(image, (self.f_x, f_y), (self.f_x + self.f_wid, f_y + f_h), (0, 255, 0), 2)
        cv2.rectangle(image, (self.focal_x, f_y), (self.focal_x + 1, f_y + 1), (255, 0, 0), 2)

        rimg = cv2.flip(image, 1)
        cv2.imshow("preview", rimg)
        cv2.waitKey(10)

