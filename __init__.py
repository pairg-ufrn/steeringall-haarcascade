from .interact import CascadeInteract


def exports():
    return CascadeInteract(
        1, 0, "assets/cascade/haarcascade_frontalface_alt.xml").enabled(True)
