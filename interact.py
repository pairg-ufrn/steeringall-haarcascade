import time
from .base import BaseCascadeInteract
from sta.definitions import GameDefinitions


class CascadeInteract(BaseCascadeInteract):

    def startup(self):
        # self.set_boundaries(self.left_boundary, self.right_boundary + 15, 120)
        super(CascadeInteract, self).startup()

    def __init__(self, thread_id, camera_index, cascade):
        super(CascadeInteract, self).__init__(thread_id, camera_index, cascade)
        self.going_right = False
        self.going_left = False
        self.is_boosting = False
        self.last_elapsed = None
        self.counter = 0
        self.time_to_update = 0.75

    def check_interaction(self):
        super(CascadeInteract, self).check_interaction()

        centered_left = self.focal_x > self.left_boundary
        centered_right = self.focal_x < self.right_boundary

        if centered_left and centered_right:
            self.going_left = False
            self.going_right = False
            self.counter = 0
            self.last_elapsed = None

        if self.focal_x > self.right_boundary:

            if self.going_left:
                if self.last_elapsed is None:
                    self.last_elapsed = time.time()

                difference = time.time() - self.last_elapsed
                self.last_elapsed = time.time()

                self.counter += difference
                if self.counter > self.time_to_update:
                    self.listener.listen(GameDefinitions.COMMAND_LEFT)
                    self.counter = 0

            else:
                self.listener.listen(GameDefinitions.COMMAND_LEFT)
                self.going_right = False
                self.going_left = True
                self.counter = 0
                self.last_elapsed = None

        elif self.focal_x < self.left_boundary:

            if self.going_right:
                if self.last_elapsed is None:
                    self.last_elapsed = time.time()

                difference = time.time() - self.last_elapsed
                self.last_elapsed = time.time()

                self.counter += difference
                if self.counter > self.time_to_update:
                    self.listener.listen(GameDefinitions.COMMAND_RIGHT)
                    self.counter = 0

            else:
                self.listener.listen(GameDefinitions.COMMAND_RIGHT)
                self.going_right = True
                self.going_left = False
                self.counter = 0
                self.last_elapsed = None

        # elif centered_left and centered_right:
        #     self.going_left = False
        #     self.going_right = False


if __name__ == "__main__":
    c = CascadeInteract(1, 0, "assets/cascade/haarcascade_frontalface_alt.xml")
    c.start()